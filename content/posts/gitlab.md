---
title: "[Gitlab] public repositories"
summary: " "
date: 2022-08-01T00:00:00
draft: false
categories: ["code"]
tags: ["code"]
---

Here are a few public projects I implemented:
- [x] A manager for json files, if you define the experiment parameters using json files, this manager may come handy. See the gitlab [page](https://gitlab.com/ey_datakalab/json_manager) or the pypi [page](https://pypi.org/project/pynput-json-manager/).
- [x] A package for tensorflow models. This implements batch-normalization folding for keras models. You can check the gitlab [page](https://gitlab.com/ey_datakalab/batch-normalization-folding) or the pypi [page](https://pypi.org/project/tensorflow-batchnorm-folding/).
- [x] A basic toturial on deep neural networks compression. Check the gitlab [page](https://gitlab.com/ey_datakalab/mla_compression).
- [x] A simple package to clean the tensorflow Bert models (to have a cleaner load in the Netron). Check the gitlab [page](https://gitlab.com/ey_datakalab/bertkerasonly).