---
title: "[Centrale-Supelec] Mathématiques pour l'IA"
summary: " "
date: 2024-09-01T00:00:00
draft: false
categories: ["teaching"]
tags: ["deep learning"]
---


new practical sessions [basics](https://colab.research.google.com/drive/1ALf1V2cFEzwNprVEi-iS0gVOaWupCxdV?usp=sharing), [mnist](https://colab.research.google.com/drive/1PNmhdsToihojcr4yAz-8DZNBWZK7qWqn?usp=sharing) and [chess](https://github.com/EdouardYvinec/chess_ai)


# Course Support
This course covers a wide range of mathematical tools for AI and science in general.We focus on Linear Algebra, Functional Analysis and Statistics.

- [Rappels](https://edouardyvinec.netlify.app/centrale/rappels.pdf)
- [Gradients](https://edouardyvinec.netlify.app/centrale/Gradients.pdf)

# exercises


Here, you will find extra exercises for the exam:
- [exercices supplémentaires (1)](https://edouardyvinec.netlify.app/centrale/exos_supp.pdf) some exercises on confusion matrix and CLT.
- [exercices supplémentaires (2)](https://edouardyvinec.netlify.app/centrale/physique_supp.pdf) these exercises may be useful for the class of physics and in general to push the notions on gradients one step further.
- [chain rule](https://edouardyvinec.netlify.app/centrale/exercice_chain_rule.pdf) deux exercises sur la chain rule.
- [exercices corriges supplementaires](https://edouardyvinec.netlify.app/centrale/exos_supp_corr.pdf)
- [examen like](https://edouardyvinec.netlify.app/centrale/2024_enonces.pdf) et [correction](https://edouardyvinec.netlify.app/centrale/exos_supp_corr.pdf)
- [more examen-like](https://edouardyvinec.netlify.app/centrale/2024_annale.pdf)

# QCMs:

| 2022 | 2023 | 2023 (corr) | 2024 | 2024 (corr) |
| :---: | :---: | :---: | :---: | :---: |
| [QCM1](https://edouardyvinec.netlify.app/centrale/QCM1.pdf) | [QCM1](https://edouardyvinec.netlify.app/centrale/qcm_2023/1.pdf) | [QCM1](https://edouardyvinec.netlify.app/centrale/qcm_2023/1_corr.pdf) | [QCM1](https://edouardyvinec.netlify.app/centrale/qcm_2024/q1.pdf) | [QCM1](https://edouardyvinec.netlify.app/centrale/qcm_2024/q1_correction.pdf) |
| [QCM2](https://edouardyvinec.netlify.app/centrale/QCM2.pdf) | [QCM2](https://edouardyvinec.netlify.app/centrale/qcm_2023/2.pdf) | [QCM2](https://edouardyvinec.netlify.app/centrale/qcm_2023/2_corr.pdf) | [QCM2](https://edouardyvinec.netlify.app/centrale/qcm_2024/q2.pdf) | [QCM2](https://edouardyvinec.netlify.app/centrale/qcm_2024/q2_correction.pdf) |
| [QCM3](https://edouardyvinec.netlify.app/centrale/QCM3.pdf) | [QCM3](https://edouardyvinec.netlify.app/centrale/qcm_2023/3.pdf) | [QCM3](https://edouardyvinec.netlify.app/centrale/qcm_2023/3_corr.pdf) | [QCM3](https://edouardyvinec.netlify.app/centrale/qcm_2024/q3.pdf) | [QCM3](https://edouardyvinec.netlify.app/centrale/qcm_2024/q3_correction.pdf) |
| [QCM4](https://edouardyvinec.netlify.app/centrale/QCM4.pdf) | [QCM4](https://edouardyvinec.netlify.app/centrale/qcm_2023/4.pdf) | [QCM4](https://edouardyvinec.netlify.app/centrale/qcm_2023/4_corr.pdf) | [QCM4](https://edouardyvinec.netlify.app/centrale/qcm_2024/q4.pdf) | [QCM4](https://edouardyvinec.netlify.app/centrale/qcm_2024/q4_correction.pdf) |
| [QCM5](https://edouardyvinec.netlify.app/centrale/QCM5.pdf) | [QCM5](https://edouardyvinec.netlify.app/centrale/qcm_2023/5.pdf) | [QCM5](https://edouardyvinec.netlify.app/centrale/qcm_2023/5_corr.pdf) | [QCM5](https://edouardyvinec.netlify.app/centrale/qcm_2024/q5.pdf) | [QCM5](https://edouardyvinec.netlify.app/centrale/qcm_2024/q5_correction.pdf) |
| [QCM6](https://edouardyvinec.netlify.app/centrale/QCM6.pdf) | [QCM6](https://edouardyvinec.netlify.app/centrale/qcm_2023/6.pdf) | [QCM6](https://edouardyvinec.netlify.app/centrale/qcm_2023/6_corr.pdf) | [QCM6](https://edouardyvinec.netlify.app/centrale/qcm_2024/q6.pdf) | [QCM6](https://edouardyvinec.netlify.app/centrale/qcm_2024/q6_correction.pdf) |
| [QCM7](https://edouardyvinec.netlify.app/centrale/QCM7.pdf) | [QCM7](https://edouardyvinec.netlify.app/centrale/qcm_2023/7.pdf) | [QCM7](https://edouardyvinec.netlify.app/centrale/qcm_2023/7_corr.pdf) | [QCM7](https://edouardyvinec.netlify.app/centrale/qcm_2024/q7.pdf) | [QCM7](https://edouardyvinec.netlify.app/centrale/qcm_2024/q7_correction.pdf) |
| [QCM8](https://edouardyvinec.netlify.app/centrale/QCM8.pdf) | [QCM8](https://edouardyvinec.netlify.app/centrale/qcm_2023/8.pdf) | [QCM8](https://edouardyvinec.netlify.app/centrale/qcm_2023/8_corr.pdf) | [QCM8](https://edouardyvinec.netlify.app/centrale/qcm_2024/q8.pdf) | [QCM8](https://edouardyvinec.netlify.app/centrale/qcm_2024/q8_correction.pdf) |
| -                                                           | [QCM9](https://edouardyvinec.netlify.app/centrale/qcm_2023/9.pdf) | [QCM9](https://edouardyvinec.netlify.app/centrale/qcm_2023/9_corr.pdf) | [QCM9](https://edouardyvinec.netlify.app/centrale/qcm_2024/q9.pdf) | [QCM9](https://edouardyvinec.netlify.app/centrale/qcm_2024/q9_corr.pdf) |

| exam 2021 | exam 2022 | exam 2023 |
| :---: | :---: | :---: |
| [exam](https://edouardyvinec.netlify.app/centrale/annale_ia_2022.pdf), [practice](https://edouardyvinec.netlify.app/centrale/exercices_supplementaires.pdf) | [exam](https://edouardyvinec.netlify.app/centrale/correction2022.pdf) | [exam](https://edouardyvinec.netlify.app/centrale/exam_2023.pdf) |

## Practical Sessions
First, we need to install **wget**.
### Install wget on Windows
google is your friend not me :)
### Install wget on Linux
{{< highlight html >}}
apt-get install wget
{{< /highlight >}}
### Install wget on MacOS

First, we need to install [homebrew](https://brew.sh/)
{{< highlight html >}}
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
{{< /highlight >}}
Then use brew to install wget on your mac.
{{< highlight html >}}
brew install wget
{{< /highlight >}}

## Notebooks
Notebooks contain a small set of exercices to practice the different notions sutided in class. If you find any mistake please [mail me](mailto:edouardyvinec@hotmail.fr).

{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/notebooks/algèbre.ipynb
wget https://edouardyvinec.netlify.app/centrale/notebooks/Classification.ipynb
wget https://edouardyvinec.netlify.app/centrale/notebooks/EDO.ipynb
wget https://edouardyvinec.netlify.app/centrale/notebooks/Fonctions_de_plusieurs_variables.ipynb
wget https://edouardyvinec.netlify.app/centrale/notebooks/Probabilités.ipynb
wget https://edouardyvinec.netlify.app/centrale/notebooks/TCL.ipynb
wget https://edouardyvinec.netlify.app/centrale/notebooks/tests_d'hypothèses.ipynb
{{< /highlight >}}


## TP - Epidemic Modelisation
The base files for the TP are:
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/epidemic/exercice.py
wget https://edouardyvinec.netlify.app/centrale/tp/epidemic/exercice_initial.py
{{< /highlight >}}
To visualize the expected results:
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/epidemic/SIR.png
wget https://edouardyvinec.netlify.app/centrale/tp/epidemic/SIRVD.png
wget https://edouardyvinec.netlify.app/centrale/tp/epidemic/SIRVDV.png
{{< /highlight >}}
Download the solution: 
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/epidemic/main.py
wget https://edouardyvinec.netlify.app/centrale/tp/epidemic/initial.py
{{< /highlight >}}
## TP - Solar System
The base files for the TP are:
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/animate.py
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/clear.py
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/data.py
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/main.py
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/solarsystem_exercice.py
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/utils.py
{{< /highlight >}}
To visualize the expected results:
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/minisolar/minisolar.gif
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/solar/solar.gif
{{< /highlight >}}
To convert a sequence of images to a gif:
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/gif/convertor.py
{{< /highlight >}}
Download the solution: *to come*
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/solar_system/solarsystem_sol.py
{{< /highlight >}}
## TP - Real Estate
The base files for the TP are:
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/real_estate/housing.csv
wget https://edouardyvinec.netlify.app/centrale/tp/real_estate/main.py
wget https://edouardyvinec.netlify.app/centrale/tp/real_estate/neural_network_exercice.py
wget https://edouardyvinec.netlify.app/centrale/tp/real_estate/read_data.py
{{< /highlight >}}
Download the solution: *to come*
## TP - Gaussian Mixture Model
The base files for the TP are:
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/mixture_of_gaussians/main.py
wget https://edouardyvinec.netlify.app/centrale/tp/mixture_of_gaussians/ME_exercice_initial.py
wget https://edouardyvinec.netlify.app/centrale/tp/mixture_of_gaussians/data.py
{{< /highlight >}}
To visualize the expected results:
{{< highlight html >}}
wget https://edouardyvinec.netlify.app/centrale/tp/mixture_of_gaussians/dataset.png
{{< /highlight >}}
Download the solution: *to come*
