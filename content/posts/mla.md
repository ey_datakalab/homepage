---
title: "[MLA] Machine Learning Avancé"
summary: " "
date: 2023-09-01T00:00:00
draft: false
categories: ["teaching"]
tags: ["deep learning"]
---

# Notes

- [slides](https://edouardyvinec.netlify.app/mla/slides.pdf)
- [notes](https://edouardyvinec.netlify.app/mla/notes.pdf)

# Practical Session

[TP compression](https://gitlab.com/ey_datakalab/mla_compression)
$\rightarrow$ [solution in torch and tensorflow]()

[TP pose estimation](https://github.com/EdouardYvinec/IamStickman)

[TP instance segmentation](https://gitlab.com/ey_datakalab/advancedmachinelearning_compression)