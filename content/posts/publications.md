---
title: "Academic Publications"
summary: " "
date: 2023-12-01T00:00:00
draft: false
categories: ["teaching"]
tags: ["publications"]
---


| Publication Name | Key-words | Status | Link |
| --- | --- | --- | --- |
| BaN-OFF | layer folding | **IJCAI 2022** | [(arxiv)](https://arxiv.org/pdf/2203.14646.pdf) |
| NUPES | quantization | **TPAMI** | [(arxiv)](https://arxiv.org/pdf/2308.05600.pdf) |
| JLCM | quantization | **ICLR 2024** | [(arxiv)](https://arxiv.org/pdf/2309.17361v1.pdf) |
| GPTQ | quantization | *under review* | [(arxiv)](https://arxiv.org/pdf/2308.07662.pdf) |
| REx | quantization | **NeurIPS 2023** | [(arxiv)](https://arxiv.org/pdf/2203.14645.pdf) |
| FOQ | quantization | **ICIP 2023** | [(arxiv)](https://arxiv.org/pdf/2303.11803.pdf) |
| T-REx | quantization | **ICIP 2023** | [(arxiv)](https://arxiv.org/pdf/2306.17442.pdf) |
| PowerQuant | quantization | **ICLR 2023** | [(arxiv)](https://arxiv.org/pdf/2301.09858.pdf)|
| SPIQ | quantization | **WACV 2023** | [(arxiv)](https://arxiv.org/pdf/2203.14642.pdf) |
| SInGE | pruning | **NeurIPS 2022** | [(arxiv)](https://arxiv.org/pdf/2207.04089.pdf) |
| RED++ | pruning | **TPAMI** |[(arxiv)](https://arxiv.org/pdf/2110.01397.pdf)|
| RED | pruning | **NeurIPS 2021** | [(neurips)](https://proceedings.neurips.cc/paper/2021/file/ae5e3ce40e0404a45ecacaaf05e5f735-Paper.pdf) |
| SAfER | robustness | *under review* | [(arxiv)](https://arxiv.org/pdf/2308.04753.pdf) |
| DeeSCO | gaze tracking | **FG 2020** |  [(arxiv)](https://arxiv.org/pdf/2004.07098.pdf) |




