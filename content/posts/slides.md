---
title: "[Epita] teaching support documents"
summary: " "
date: 2024-10-01T00:00:00
draft: false
categories: ["teaching"]
tags: ["slides", "notebooks"]
---

# Deep Neural Networks

Slides for the course: [[Epita] Deep Neural Networks Course](https://edouardyvinec.netlify.app/posts/dnn_course/):
- [Introduction](https://edouardyvinec.netlify.app/presentations/slides/01_intro_to_deep_learning/index.html)
- [Back Propagation](https://edouardyvinec.netlify.app/presentations/slides/02_backprop/index.html)
- [Convolutional Neural Networks](https://edouardyvinec.netlify.app/presentations/slides/04_conv_nets/index.html)
- [Detection / Segmentation](https://edouardyvinec.netlify.app/presentations/slides/05_conv_nets_2/index.html)
- [Generative Models](https://edouardyvinec.netlify.app/presentations/slides/10_unsupervised_generative_models/index.html)
- [Transformers and MLP-Mixers](https://edouardyvinec.netlify.app/presentations/slides/11_new_architectures/index.html)
- [Videos](https://edouardyvinec.netlify.app/presentations/slides/13_videos/index.html)
- [DALL-E 2](https://edouardyvinec.netlify.app/presentations/slides/12_Dalle2/index.html)
- [Overview](https://edouardyvinec.netlify.app/presentations/slides/14_overview/index.html)
- [3D Scenes](https://edouardyvinec.netlify.app/presentations/slides/15_nerf/index.html)

NERF tutorial: [gitlab page](https://gitlab.com/ey_datakalab/nerf-practical-session)

These slides are based on another [course](https://m2dsupsdlclass.github.io/lectures-labs/) by Olivier Grisel and Charles Ollion.

Notebooks for the course: [[Epita] Deep Neural Networks Course](https://edouardyvinec.netlify.app/posts/dnn_course/):
{{< highlight html >}}
wget https://deepcourse-epita.netlify.app/notebooks/Backpropagation_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/Autograd_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/Convolutions_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/CNN_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/Transfer_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/Localization_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/Segmentation_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/Metric_Learning_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/Domain_Adaptation_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/VAE_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/GAN_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/Transformer_Deepcourse.ipynb
wget https://deepcourse-epita.netlify.app/notebooks/MLP_Mixer_Deepcourse.ipynb
{{< /highlight >}}



These notebooks are based on the material provided by [Arthur Douillard](https://arthurdouillard.com/).