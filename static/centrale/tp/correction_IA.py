def diag(v: Vector) -> Matrix:
    """
    v est un vecteur.

    on va creer la matrice diagonale a partir du vector v
    """
    return [[v[i] if i == j else 0 for j in range(len(v))] for i in range(len(v))]


def matmul(m1: Matrix, m2: Matrix) -> Matrix:
    """
    m1 est une matrice.
    m2 est une matrice.

    on calcule le produit matriciel de m1 et m2
    """
    assert len(m1[0]) == len(m2)
    return [[sum([m1[i][k] * m2[k][j] for k in range(len(m1[0]))]) for j in range(len(m2[0]))] for i in range(len(m1))]


def matvecmul(m1: Matrix, m2: Vector) -> Matrix:
    """
    m1 est une matrice.
    m2 est un vecteur.

    on calcule le produit matriciel de m1 et m2
    """
    return [sum([m1[i][k] * m2[k] for k in range(len(m1[0]))]) for i in range(len(m1))]


class ReLU(Module):
    def __init__(self):
        super().__init__()

    def forward(self, x: Vector) -> Vector:
        self.context = x
        return [max(0, xi) for xi in x]

    def backward(self, J: Matrix) -> Matrix:
        return matmul(J, diag([1 if c > 0 else 0 for c in self.context]))


class Linear(Module):
    def __init__(self, n: int, m: int):
        super().__init__()
        self.n = n
        self.m = m
        self.W = [[random.gauss(0, 1) for _ in range(m)] for _ in range(n)]

    def forward(self, x: Vector) -> Vector:
        self.context = x
        return matvecmul(self.W, x)

    def backward(self, J: Matrix) -> Matrix:
        return matmul(J, self.W)

    def convert_context(self) -> Matrix:
        return [
            [self.context[j % self.m] if j // self.m == i else 0 for j in range(self.n * self.m)] for i in range(self.n)
        ]

    def backward_w(self, J: Matrix) -> Matrix:
        return matmul(J, self.convert_context())
