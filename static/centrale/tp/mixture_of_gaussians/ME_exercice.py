"""
read this https://perso.telecom-paristech.fr/bonald/documents/gmm.pdf
"""

import numpy as np
from typing import Any
from data import show_dataset


class GMM:
    def __init__(self, number_of_clusters: int) -> None:
        self.number_of_clusters = number_of_clusters
        self.is_initialized = False
        self.step = 0

    def initialisation(self, X: np.ndarray):
        self.dataset_mean = np.mean(X, axis=0)
        self.dataset_var = np.cov(np.transpose(X))
        self.mu = X[np.random.choice(X.shape[0], self.number_of_clusters)]
        self.alpha = np.ones(shape=(self.number_of_clusters,))
        self.sigma = np.repeat(
            self.dataset_var[np.newaxis, :, :] / self.number_of_clusters,
            self.number_of_clusters,
            axis=0,
        )

    def expectation(self, X: np.ndarray):
        ### To Complete ###
        pass

    def maximization(self, X: np.ndarray):
        ### To Complete ###
        pass

    def __call__(self, X: np.ndarray, *args: Any, **kwds: Any) -> Any:
        if not self.is_initialized:
            self.initialisation(X)
            self.is_initialized = True
        self.expectation(X)
        show_dataset(X, self.p, self.step)
        self.maximization(X)
        self.step += 1
