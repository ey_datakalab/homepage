from ME_exercice import *
from data import *

if __name__ == "__main__":
    X, Y = get_dataset(num_centers=3)
    show_dataset(X, Y, None)
    model = GMM(number_of_clusters=3)
    for k in range(100):
        print(f"\r{k}/1000", end="")
        model(X)
    print()
