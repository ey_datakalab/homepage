"""
read this https://perso.telecom-paristech.fr/bonald/documents/gmm.pdf
"""

import numpy as np
from typing import Any
from data import show_dataset


class GMM:
    def __init__(self, number_of_clusters: int) -> None:
        self.number_of_clusters = number_of_clusters
        self.is_initialized = False
        self.step = 0

    def initialisation(self, X: np.ndarray):
        self.dataset_mean = np.mean(X, axis=0)
        self.dataset_var = np.cov(np.transpose(X))
        self.mu = X[np.random.choice(X.shape[0], self.number_of_clusters)]
        self.alpha = np.ones(shape=(self.number_of_clusters,))
        self.sigma = np.repeat(
            self.dataset_var[np.newaxis, :, :] / self.number_of_clusters,
            self.number_of_clusters,
            axis=0,
        )

    def expectation(self, X: np.ndarray):
        self.p = np.zeros(shape=(len(X), self.number_of_clusters))
        for i, x in enumerate(X):
            s = 0
            for j in range(self.number_of_clusters):
                self.p[i, j] = (
                    self.alpha[j]
                    / np.sqrt(np.abs(np.linalg.det(self.sigma[j])))
                    * np.exp(
                        -0.5
                        * np.transpose(x - self.mu[j])
                        @ np.linalg.inv(self.sigma[j])
                        @ (x - self.mu[j])
                    )
                )
                s += self.p[i, j]
            for j in range(self.number_of_clusters):
                if s > 0:
                    self.p[i, j] = self.p[i, j] / s

    def maximization(self, X: np.ndarray):
        for j in range(self.number_of_clusters):
            self.alpha[j] = 0
            self.mu[j] = np.zeros_like(self.mu[j])
            for i, x in enumerate(X):
                self.alpha[j] += self.p[i, j]
                self.mu[j] += self.p[i, j] * x
            self.mu[j] = self.mu[j] / self.alpha[j]
            self.sigma[j] = np.zeros_like(self.sigma[j])
            self.sigma[j] = (
                np.dot(self.p[:, j] * (X - self.mu[j]).T, (X - self.mu[j]))
                / self.alpha[j]
            )

    def __call__(self, X: np.ndarray, *args: Any, **kwds: Any) -> Any:
        if not self.is_initialized:
            self.initialisation(X)
            self.is_initialized = True
        self.expectation(X)
        show_dataset(X, self.p, self.step)
        self.maximization(X)
        self.step += 1
