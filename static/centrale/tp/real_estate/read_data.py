import numpy as np
import csv
import random
from typing import Tuple

keys = [
    "CRIM",
    "ZN",
    "INDUS",
    "CHAS",
    "NOX",
    "RM",
    "AGE",
    "DIS",
    "RAD",
    "TAX",
    "PTRATIO",
    "B",
    "LSTAT",
    "SalesPrice",
]


def get_data_from_csv() -> np.ndarray:
    data = []
    with open("housing.csv", newline="") as csvfile:
        spamreader = csv.reader(csvfile)
        for row in spamreader:
            data.append(list(filter(None, row[0].split(" "))))
    data = np.array(data)
    return data


def split_data(data: np.ndarray) -> Tuple[np.ndarray, np.ndarray]:
    data = data.astype(float)
    m = np.min(data, axis=0)
    M = np.max(data, axis=0)
    data = (data - m) / (M - m)
    random.seed(42)
    random.shuffle(data)
    test_data = data[: int(len(data) * 0.2), :]
    train_data = data[int(len(data) * 0.2) :, :]
    return test_data, train_data
