class nombre:
    def __init__(self, value: float) -> None:
        self.value = value

    def add(self, value_to_add: float) -> None:
        self.value = self.value + value_to_add

    def substract(self, value_to_substract):
        self.value = self.value - value_to_substract


# a = nombre(value=4)
# print(a.value)
# a.add(2)
# print(a.value)
# a.substract(2)
# print(a.value)


class voiture:
    def __init__(
        self, wheels: int, doors: int, motor_power: float, mass: float = 1
    ) -> None:
        self.wheels = wheels
        self.doors = doors
        self.motor_power = motor_power
        self.mass = mass


def multiply_two_numbers(a: float, b: float, verbose: bool = False):
    if verbose:
        print(a * b)
    return a * b


peugeot_207 = voiture(4, 4, -1.2)
tesla = voiture(4, 4, -4.2)

# print(peugeot_207.wheels, peugeot_207.doors, peugeot_207.motor_power)
# print(peugeot_207)
# print(tesla.wheels, tesla.doors, tesla.motor_power)
# print(tesla)

multiply_two_numbers(2, 2)
multiply_two_numbers(2, 3, True)
multiply_two_numbers(2, 2, False)
