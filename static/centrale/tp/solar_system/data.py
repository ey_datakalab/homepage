"""
Durée d'une orbite de Mercure: 88 jours
Durée d'une orbite de Venus: 225 jours
Durée d'une orbite de la Terre: 365 jours
Durée d'une orbite de Mars: 687 jours

Une itération correspond à 100.000 secondes alros qu'une journée en dure 86400.
En conséquence, on devrait observer:
Durée d'une orbite de Mercure: 76 itérations
Durée d'une orbite de Venus: 194 itérations
Durée d'une orbite de la Terre: 315 itérations
Durée d'une orbite de Mars: 593 itérations
"""

import numpy as np
from typing import Tuple


def solar_system_raw_data() -> Tuple[
    np.ndarray,
    np.ndarray,
    np.ndarray,
    np.ndarray,
    np.ndarray,
]:
    """
    We set the initial parameters for our Solar System with the main planets.
    For each element we provide the distance to the sun,
    the absolute speed the mass and define visualization parameters.
    """
    distance_planet_sun = np.array(
        [
            0.0,  # Sun
            58e9,  # Mercury
            108e9,  # Venus
            149.5e9,  # Earth
            228e9,  # Mars
            779e9,  # Jupiter
            1426.5e9,  # Saturn
            2871e9,  # Uranus
            4498.5e9,  # Neptune
        ]
    )
    absolute_speed = np.array(
        [
            0.0,  # Sun
            47.9e3,  # Mercury
            35e3,  # Venus
            29.8e3,  # Earth
            24.1e3,  # Mars
            13.1e3,  # Jupiter
            9.7e3,  # Saturn
            6.8e3,  # Uranus
            5.4e3,  # Neptune
        ]
    )
    masses = np.array(
        [
            1.988435e30,  # Sun
            3.302e23,  # Mercury
            4.8685e24,  # Venus
            5.9742e24,  # Earth
            6.4185e23,  # Mars
            1.899e27,  # Jupiter
            5.6846e26,  # Saturn
            8.6832e25,  # Uranus
            1.0243e26,  # Neptune
        ]
    )
    colors = [
        [1, 1, 0.25],  # Sun
        [1, 0.5, 0.5],  # Mercury
        [0.75, 0.75, 0],  # Venus
        [0, 0, 1],  # Earth
        [1, 0, 0],  # Mars
        [1, 0.25, 0],  # Jupiter
        [0, 0.75, 1],  # Saturn
        [0, 0.25, 0.5],  # Uranus
        [0.5, 0.75, 0.5],  # Neptune
    ]
    radius = [
        50,  # Sun
        5,  # Mercury
        5,  # Venus
        10,  # Earth
        7,  # Mars
        40,  # Jupiter
        35,  # Saturn
        20,  # Uranus
        20,  # Neptune
    ]
    return (distance_planet_sun, absolute_speed, masses, colors, radius)


def mini_solar_system_raw_data() -> Tuple[
    np.ndarray,
    np.ndarray,
    np.ndarray,
    np.ndarray,
    np.ndarray,
]:
    """
    We set the initial parameters for our Solar System with the four closest planets.
    For each element we provide the distance to the sun,
    the absolute speed the mass and define visualization parameters.
    """
    distance_planet_sun = np.array(
        [
            0.0,  # Sun
            58e9,  # Mercury
            108e9,  # Venus
            149.5e9,  # Earth
            228e9,  # Mars
        ]
    )
    absolute_speed = np.array(
        [
            0.0,  # Sun
            47.9e3,  # Mercury
            35e3,  # Venus
            29.8e3,  # Earth
            24.1e3,  # Mars
        ]
    )
    masses = np.array(
        [
            1.988435e30,  # Sun
            3.302e23,  # Mercury
            4.8685e24,  # Venus
            5.9742e24,  # Earth
            6.4185e23,  # Mars
        ]
    )
    colors = [
        [1, 1, 0.25],  # Sun
        [1, 0.5, 0.5],  # Mercury
        [0.75, 0.75, 0],  # Venus
        [0, 0, 1],  # Earth
        [1, 0, 0],  # Mars
    ]
    radius = [
        50,  # Sun
        5,  # Mercury
        5,  # Venus
        10,  # Earth
        7,  # Mars
    ]
    return (distance_planet_sun, absolute_speed, masses, colors, radius)


def two_suns_system() -> Tuple[
    np.ndarray,
    np.ndarray,
    np.ndarray,
    np.ndarray,
    np.ndarray,
]:
    """
    Simple system with two stars and nothing else
    """
    distance_planet_sun = np.array(
        [
            0.0,  # Sun 1
            5.9742e12,  # Sun 2
        ]
    )
    absolute_speed = np.array(
        [
            0.0,  # Sun 1
            0.0,  # Sun 2
        ]
    )
    masses = np.array(
        [
            1.988435e35,  # Sun 1
            1.988435e35,  # Sun 2
        ]
    )
    colors = [
        [1, 1, 0.25],  # Sun 1
        [1, 1, 0.75],  # Sun 2
    ]
    radius = [
        50,  # Sun 1
        50,  # Sun 2
    ]
    return (distance_planet_sun, absolute_speed, masses, colors, radius)


def three_suns_system() -> Tuple[
    np.ndarray,
    np.ndarray,
    np.ndarray,
    np.ndarray,
    np.ndarray,
]:
    """
    Simple system with three stars and nothing else
    """
    distance_planet_sun = np.array(
        [
            0.0,  # Sun 1
            5.9742e14,  # Sun 2
            5.9742e14,  # Sun 3
        ]
    )
    absolute_speed = np.array(
        [
            9.7e7,  # Sun 1
            9.7e7,  # Sun 2
            9.7e7,  # Sun 3
        ]
    )
    masses = np.array(
        [
            1.988435e30,  # Sun 1
            1.988435e30,  # Sun 2
            1.988435e30,  # Sun 3
        ]
    )
    colors = [
        [1, 1, 0.25],  # Sun 1
        [1, 1, 0.75],  # Sun 2
        [1, 0.5, 0.75],  # Sun 3
    ]
    radius = [
        50,  # Sun 1
        50,  # Sun 2
        50,  # Sun 3
    ]
    return (distance_planet_sun, absolute_speed, masses, colors, radius)
