"""
Ce programme a pour but d'étudier:
    les fonctions à plusieurs variables
    les méthodes numériques
    les dérivées
    et un peu de calcul matriciel (très peu)

Nous avons plusieurs configurations testables:
    1. le système solaire avec le soleil et les huit planètes principales
    2. une version simplifiée du système solaire (Soleil, Mercure, Venus, Terre et Mars)
    3. un système à deux étoiles
    4. un systèmes à trois étoiles

Les données initialies peuvent être éditées dans le fichier data.py.
L'execution se fait sur la base de deux paramètres:
    -n <int> pour le nombre d'itérations de la simulations
    --sys <str> pour désigner le système à charger
"""


from re import T
import numpy as np
import data as data
import solarsystem_exercice as solar
import utils as utils
import argparse
import animate
import clear
import os


if __name__ == "__main__":
    system_options = {
        "solar": data.solar_system_raw_data,
        "minisolar": data.mini_solar_system_raw_data,
        "twosuns": data.two_suns_system,
        "threesuns": data.three_suns_system,
    }
    parser = argparse.ArgumentParser(
        usage=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter
    )
    parser.add_argument(
        "-n",
        nargs="?",
        type=int,
        default=100,
        help="number of iterations in the simulation",
    )
    parser.add_argument(
        "--sys",
        nargs="?",
        type=str,
        default="solar",
        choices=list(system_options.keys()),
        help="system to simulate",
    )
    args = parser.parse_args()

    np.random.seed(0)
    num_frames = args.n
    (
        distance_planet_sun,
        absolute_speed,
        masses,
        colors,
        radius,
    ) = system_options[args.sys]()

    initial_p = utils.convert_distance_to_positions(distances=distance_planet_sun)
    initial_s = utils.convert_absolute_speeds(
        speeds=absolute_speed, positions=initial_p
    )
    test_system = solar.SolarSystem(
        initial_p=initial_p, initial_s=initial_s, masses=masses, delta=1e3
    )
    if args.sys not in os.listdir():
        os.mkdir(args.sys)

    animate.perform_simulation(
        folder=args.sys,
        num_frames=num_frames,
        num_planets=len(distance_planet_sun),
        colors=colors,
        radius=radius,
        distance_planet_sun=distance_planet_sun,
        test_system=test_system,
    )
    clear.remove_chache_folders()
