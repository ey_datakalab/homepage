import numpy as np


def convert_distance_to_positions(distances: np.ndarray) -> np.ndarray:
    """
    from distances to the sun we randomly sample cartesian positions.
    To do so we sample an angle in 0; 2pi
    """
    positions = np.zeros(shape=(len(distances), 2))
    for cpt in range(len(positions)):
        theta = 2 * np.pi * np.random.uniform()
        positions[cpt, 0] = np.cos(theta) * distances[cpt]
        positions[cpt, 1] = np.sin(theta) * distances[cpt]
    return positions


def convert_absolute_speeds(speeds: np.ndarray, positions: np.ndarray) -> np.ndarray:
    """
    convert absolute speed to orbital speed in cartesian coordinate system
    """
    output = np.zeros(shape=(len(speeds), 2))
    rot_matrix = np.array(
        [
            [0, 1],
            [-1, 0],
        ]
    )
    for cpt in range(len(output)):
        X = np.copy(positions[cpt] / max(np.sqrt(np.sum(positions[cpt] ** 2)), 1))
        output[cpt] = speeds[cpt] * (rot_matrix @ X)
    return output
